package com.datadrivenAssignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisteringUsingJxl {
	public static void main(String args[]) throws BiffException, IOException
	{
	WebDriver driver=new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
	driver.findElement(By.linkText("Register")).click();
	driver.findElement(By.id("gender-female")).click();
	File f=new File("/home/syed/Documents/TestData/Rege.xls");
	FileInputStream fis=new FileInputStream(f);
	Workbook wb=Workbook.getWorkbook(fis);
	Sheet s=wb.getSheet("Sheet1");
	int rows=s.getRows();
	int colums=s.getColumns();
	for(int i=1;i<rows;i++) {
		
			 String first=s.getCell(0,i).getContents();
			 String last=s.getCell(1, i).getContents();
			 String email=s.getCell(2,i).getContents();
			 String password=s.getCell(3, i).getContents();
			 String currentpassword=s.getCell(3, i).getContents();
			 driver.findElement(By.id("gender-female")).click();
				driver.findElement(By.id("FirstName")).sendKeys(first);
				driver.findElement(By.id("LastName")).sendKeys(last);
			 driver.findElement(By.id("Email")).sendKeys(email);
				driver.findElement(By.id("Password")).sendKeys(password);
				driver.findElement(By.id("ConfirmPassword")).sendKeys(currentpassword);
				driver.findElement(By.id("register-button")).click();
				driver.close();
		
	}
	}

}
