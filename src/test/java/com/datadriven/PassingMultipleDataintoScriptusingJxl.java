package com.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PassingMultipleDataintoScriptusingJxl {
	public static void main(String args[]) throws BiffException, IOException
	{
	WebDriver driver=new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
	WebElement login = driver.findElement(By.linkText("Log in"));
	login.click();
	File f=new File("/home/syed/Documents/TestData/TestDataone.xls");
	FileInputStream fis=new FileInputStream(f);
	Workbook wb=Workbook.getWorkbook(fis);
	Sheet s=wb.getSheet("Sheet1");
	int rows=s.getRows();
	int colums=s.getColumns();
	for(int i=1;i<rows;i++) {
		
			 String username=s.getCell(0,i).getContents();
			 String password=s.getCell(1, i).getContents();
			 driver.findElement(By.id("Email")).sendKeys(username);
				driver.findElement(By.id("Password")).sendKeys(password);
				driver.findElement(By.xpath("//input[@value='Log in']")).click();
				driver.findElement(By.linkText("Log out")).click();
				//driver.close();
		
	}
	}

}
